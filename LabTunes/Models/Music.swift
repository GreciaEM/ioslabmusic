//
//  Music.swift
//  LabTunes
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//

import Foundation

class Music {
    static var urlSession = URLSession(configuration: .default)
    
    static func fetchSongs(songName: String = "Metallica", onSuccess: @escaping ([Song]) -> Void) { // el escaping indica en qué momento se va a ejecutar el closure (antes de terminar la función o después) escaping dice que ese escaping se ejecuta después del codigo de la función (es como un promise)
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=\(songName)")
        
        let dataTask = urlSession.dataTask(with: url!) { data, response, error in
            if error == nil {
                guard let  statusCode = (response as? HTTPURLResponse)?.statusCode else {return}
                if statusCode == 200 {
                    guard let json = parseData(data: data!) else {return}
                    let songs = songsFrom(json: json)
                    onSuccess(songs)
                }
            }
        }
        dataTask.resume()
    }
    
    static func parseData (data: Data) -> NSDictionary? {
        let json = try! JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
        return json
    }
    
    static func songsFrom(json: NSDictionary) -> [Song] {
        let results = json["results"] as! [NSDictionary]
        var songs: [Song] =  []
        for dataResult in results {
            let song = Song.create(dict: dataResult)
            songs.append(song!)
        }
        return songs
    }
}
