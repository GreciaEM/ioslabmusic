//
//  User.swift
//  LabTunes
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//

import Foundation

class User {
    static let userName = "iOSLab"
    static let password = "password"
    static let session = Session.sharedInstance
    // se hace método de clase porque no se debe tener la instancia si aun no tiene un usuario
    static func login (userName: String, password: String) -> Bool {
        if self.userName == userName {
            session.saveSession()
            return true
        }
        return false
    }
    
    static func fetchSongs() throws {
        guard let token = Session.sharedInstance.token else {
            throw UserError.noSessionAvailable
        }
        debugPrint(token)
    }
    enum UserError: Error {
        case noSessionAvailable
    }
}
