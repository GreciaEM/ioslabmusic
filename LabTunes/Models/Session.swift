//
//  Session.swift
//  LabTunes
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//

import Foundation

// un objeto session guarda el token que permite la conexión
// solo se permite un objeto (buenas practicas) y tmbn para que no se puedan crear muchas sesiones en la app (una para todo el ciclo de vida)

class Session: NSObject {
    var token: String?
    static let sharedInstance = Session() // singleton (solo es uno en la vida) solo permite una instancia, con let se garantiza que no se puede cambiar el valor esta cte estatica almacena el singleton (porque en algun momento se podria requerir otro). Es estatica porque se accede desde la clase.
    
    // Se garantiza que sólo se puede llamar desde aquì este singleton, no de otros lados
    override private init() {
        super.init()
    }
    
    // Sólo se debe llamar cuando es exitoso el login (eso se pone en User)
    func saveSession() {
        token = "1234567890"
    }
    
}
