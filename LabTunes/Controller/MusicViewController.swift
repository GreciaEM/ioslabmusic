//
//  MusicViewController.swift
//  LabTunes
//
//  Created by Grecia Escárcega on 11/10/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var songs: [Song] = []
    let searchController = UISearchController(searchResultsController: nil) //se le indica que la vista que hará la busqueda es la misma en la que se encuentra

    override func viewDidLoad() {
        super.viewDidLoad()
        downloadSongs()
        setupSearchBar()
        // Do any additional setup after loading the view.
    }
    
    func downloadSongs() {
        Music.fetchSongs { (result: [Song]) in
            self.songs = result
            // para ponerlo en el hilo principal. Todo corre en el hilo principal pero cuando inicia el URL session, su respuesta no la regresa en el hilo principal (si lo hiciera tardarìa mucho y se congelaria la interfaz hasta que termina de responder) Hace todo el trabajo en background pero el siguiente codigo hazlo en el pruncipal (cuando termine su tarea)
            DispatchQueue.main.async {
                self.tableView.reloadData() // así solito el download está en otro hilo, no en el principal
            }
            
        }
    }
    
    func downloadSongsByName(name: String) {
        Music.fetchSongs(songName: name) { (result: [Song]) in
            self.songs = result
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    func setupSearchBar() {
        searchController.searchResultsUpdater = self // se necesita otra extension
        searchController.searchBar.placeholder = "search songs" //lo que dirá la barrita de busqueda
        navigationItem.searchController = searchController // el navigation item (porque estamos dentro de un navigation controller) ya esta preparado para recibir un search controller
        definesPresentationContext = true //
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// para claridad de codigo se puso como un extension. Con contorladores más complejos se cerean objetos que tienen estas funciones
// se tiene que conectar al table view porque si no no entra. Checar si entra con breakpoints en el inicio de las funciones
extension MusicViewController: UITableViewDataSource {
    
    // indica el numero de celdas que tendrá la tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    // para pedir una celda en cierto undex path
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "musicCell", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row].artist
        return cell
    }
    
    
}

extension MusicViewController: UISearchResultsUpdating {
    //esta se manda a llamar cuando se empuece a escribir texto
    func updateSearchResults(for searchController: UISearchController) {
        downloadSongsByName(name: searchController.searchBar.text!)
    }
    
}
