//
//  NetworkTests.swift
//  LabTunesTests
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//

import XCTest
@testable import LabTunes //podemos acceder a todos

class NetwrokTests: XCTestCase {
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: .default)
    }
    
    func testValidCallToItunes() {
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=Queen")
        var statusCode: Int?
        var responseError: Error?
        
        let promise = expectation(description: "Handler Invoked") // para que espere a que conteste el servidor antes de continuar con el codigo
        
        //esta seccion se ejecuta hasta que el servidor conteste (si contesta)
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill() // indica que la promesa se cumplio si no se pone, se queda ahì y entra el timeout. Si algo se pone mal en la url se cumple la promesa pero con errores y ya no pasa la prueba.
        }
        // Este se ejecuta antes de que el servidor conteste por lo que falla la prueba. Para esto se pone una promise (la de arriba)
        dataTask.resume() // para ya empezar con la conexion
        waitForExpectations(timeout: 5, handler: nil) //termina todas las promise antes de continuar con las pruebas, así ya pasa la prueba. El timeout sirve para no esperar eternamente a que conteste (por si algo falla con el servidor o se escribio mal...)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    // en esta prueba, en caso de haber error, no cumple la promise pero truena la prueba. en caso de que no haya error
    func testSlowValidCallToItunes() {
        let url = URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=Queen")
        let promise = expectation(description: "Status code: 200")
        
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            if let error = error {
                XCTFail("Error: (\(error.localizedDescription)") // si hay error detiene la prueba, no espera al timeout
            } else if let  statusCode = (response as? HTTPURLResponse)?.statusCode { //valida que el status code esté en el response
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
