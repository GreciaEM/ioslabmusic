//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//
// para pruebas unitarias, se tiene que poner test como incio de cada funcion!!

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        // Cada que haga una prueba se ponen estos valores
        let session = Session.sharedInstance
        session.token = nil
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCorrectLogin() {
        XCTAssertTrue(User.login(userName: "iOSLab", password: "test"))
    }
    
    func testWrongLogin() {
        XCTAssertFalse(User.login(userName: "grecia", password: "test"))
    }
    // cuando se le da un valor al singleton, se queda cuando sigue la app, para no tener eso, se tiene que "reiniciar" los valores para las pruebas (en setup)
    func testSaveSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "223")
        XCTAssertNotNil(session.token)
    }

    func testNoSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "mono", password: "223")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken() {
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890","Token should match") //aparece ese mensaje de para saber que pasa con la prueba ( el tercer parametro
    }
    
    func testUnexpectedToken() {
        let _ = User.login(userName: "iOSLab", password: "223")
        let session = Session.sharedInstance
        XCTAssertNotEqual(session.token!, "124567890","Token does not match")
    }
    
    func testFetchWithNoLogin() {
        XCTAssertThrowsError(try User.fetchSongs()) // el try se pone porque se puede tener una excepcion (throw), como no se sabe, se marca que se debe intentar (si no se puede, podrìa tener una excepcion)
    }
    func testMusicSongs() {
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Dowloaded")
        Music.fetchSongs { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
}
