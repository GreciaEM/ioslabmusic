//
//  LabTunesUITests.swift
//  LabTunesUITests
//
//  Created by Grecia Escárcega on 11/9/18.
//  Copyright © 2018 Grecia Escárcega. All rights reserved.
//
// Para pruebas de interfaz
// no accede a todo el código, solo a la interfaz
import XCTest

class LabTunesUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testAppLogin() {
        let app = XCUIApplication() //la app que ya esta correindo
        let nameTextField = app.textFields["userNameTextField"] // el nombre del textfield lo busca en los identificadores (ponerlo en el storyboard: cuadrito -> accesibilidad -> enable, nombre de indentificador
        nameTextField.tap()
        nameTextField.typeText("iOSLab")
        
        let passwordTextField = app.textFields["passwordTextField"] //.securetextfields por si lo puse como tipo password
        passwordTextField.tap()
        passwordTextField.typeText("pass")
        
        let loginButton = app.buttons["loginButton"]
        loginButton.tap()
        XCTAssertTrue(app.isDisplayingMusic)
    }
}

extension XCUIApplication {
    var isDisplayingMusic: Bool {
        return otherElements["MusicView"].exists
    }
    
}
